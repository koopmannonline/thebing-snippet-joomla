<?php

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );
require(dirname(__FILE__)."/class.thebingsnippet.php");

#error_reporting(E_ALL ^ E_NOTICE);
#ini_set("display_errors", 1);

/**
 * Class plgContentThebingsnippet
 * @author Thebing Services GmbH
 */
class plgContentThebingsnippet extends JPlugin {

	/**
	 * @param $context
	 * @param $article
	 * @param $params
	 * @param $limitstart
	 * @return bool
	 */
	public function onContentBeforeDisplay($context, &$article, &$params, $limitstart) {

		preg_match_all('/\[thebingsnippet (.*)\]/', $article->text, $aMatches);

		if(
			!empty($aMatches) &&
			!empty($aMatches[1])
		) {
			foreach($aMatches[1] as $iMatch => $sAttributes) {
				$sOriginalCode = $aMatches[0][$iMatch];
				$article->text = str_replace($sOriginalCode, $this->replaceCode($sAttributes), $article->text);
			}
		}

	}

    /**
     * @param string $sAttributes
     * @return string
     */
	private function replaceCode($sAttributes) {

		$aAttributes = [];
		$aValues = explode(' ', $sAttributes);
		foreach($aValues as $sValue) {
			$aKeyAndValue = explode('=', $sValue, 2);
			if(count($aKeyAndValue) == 2) {
				$aAttributes[$aKeyAndValue[0]] = trim($aKeyAndValue[1], '"');
			}
		}

		$sContent = Thebing_Joomla_Snippet::getContent($aAttributes);

        $sUrl = JUri::current();
		if(false === strpos($sUrl, '?')) {
			$sUrl .= '?';
		} elseif(mb_substr($sUrl, -1, 1) != '&') {
			$sUrl .= '&';
		}
		$sUrl = str_replace('$', '\\$', $sUrl);

		$sContent = preg_replace('=(<(?:script|img)\s+.*src\s*\=\s*\")\?(.+)(\".*>)=isUu', '$1'.$sUrl.'$2$3', $sContent);

		$sContent = preg_replace('=(<(?:a)\s+.*href\s*\=\s*\")\?(.+)(\".*>)=isUu', '$1'.$sUrl.'$2$3', $sContent);

		return $sContent;

	}

}