=== Plugin Name ===
Contributors: Fidelo Software GmbH
Stable tag: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The plugin links the forms for Fidelo School & Agency Software

== Description ==

The plugin allows you to link the forms of the Fidelo School & Agency Software into your Joomla website.

== Installation ==

1. Install the plugin "Thebing Snippet"
2. Activate the plugin through the "Plugins" menu in Joomla.

Examples:
    = If you want to show form/templates with template- and combinationkey add: =
    [thebingsnippet type="default" server="https://schoolname.fidelo.com" combinationkey="123456789" templatekey="123456789"]

    = If you want to show the placement test add: =
    [thebingsnippet type="placement_test" server="https://schoolname.fidelo.com" key="KEY-0123456789" language="en"]

    = If you want to show the registration form add: =
    [thebingsnippet type="registrationform" server="https://schoolname.fidelo.com" key="1=KEY-0123456789" language="en"]